import serial
import sys
import yaml
import pymodbus
#import statsd
import argparse
import time
from telegraf.client import TelegrafClient
#from influxdb import InfluxDBClient
from datetime import datetime
from pymodbus.pdu import ModbusRequest
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.transaction import ModbusRtuFramer
import logging
from operator import itemgetter
from itertools import groupby
import re
import bitstring

class drstk_ModbusUnit():

    def __init__(self,master,device):
        config_file = 'config.yaml' # default config
        with open(config_file, 'r', encoding='utf-8') as yaml_file:
            self.cfg = yaml.safe_load(yaml_file)

class drstk_ModbusClient():
    """
    This class is conceived as a container for the functions to be used
    in Modbus buses for the monitoring of certain industrial equipment.
    """
    def __init__(self, device='/dev/ttyUSB0', config_file='config.yaml'):
        with open(config_file, 'r', encoding='utf-8') as yaml_file:
            self.cfg = yaml.safe_load(yaml_file)
        units_cfg_file = 'units_cfg.yaml' # units config
        with open(units_cfg_file, 'r', encoding='utf-8') as yaml_file:
            self.cfg.update(yaml.safe_load(yaml_file))

        # configure the bus parameters
        stop_bits   = self.cfg['bus_params']['stop_bits']
        byte_size   = self.cfg['bus_params']['byte_size']
        parity_type = self.cfg['bus_params']['parity_type']
        baud_rate   = self.cfg['bus_params']['baud_rate']
        mthd        = self.cfg['bus_params']['method']
        if 'general' in self.cfg:
            if 'period' in self.cfg['general']:
                self.period = self.cfg['general']['period']
        else:
            self.period = 20
        self.client = ModbusClient(method = mthd, port=device ,stopbits = stop_bits, bytesize = byte_size, parity = parity_type, baudrate= baud_rate)
        self.connection = self.client.connect()
        print('connected: '+str(self.connection))

        # configure the units on the bus
        self.devices = self.cfg['units']

        # plant tag for metrics collection
        self.plant = self.cfg['bus_params']['plant']

        ## initialize statsd client
        #statsd_host = self.cfg['statsd_params']['host']
        #statsd_port = self.cfg['statsd_params']['port']
        #self.statsd_c = statsd.StatsClient(statsd_host, statsd_port)

        # initialize telegraf client
        telegraf_host = self.cfg['telegraf_params']['host']
        telegraf_port = self.cfg['telegraf_params']['port']
        self.telegraf_c = TelegrafClient(host=telegraf_host, port=telegraf_port, tags={'location': self.plant})

        # initialize influxDB client
        #influx_host = self.cfg['influx_params']['host']
        #influx_port = self.cfg['influx_params']['port']
        #influx_dbname = self.cfg['influx_params']['dbname']
        #self.influx_c = InfluxDBClient(host=influx_host, database=influx_dbname, use_udp=True, udp_port=influx_port)

    def read_params(self, param_nr, num, unit):
        address = self.cfg['units'][unit]['address']
        u_type = self.cfg['units'][unit]['unit_type']
        offset = self.cfg['unit_types'][u_type]['param_offset']
        return self.client.read_holding_registers(param_nr+offset,num,unit=address)

    def display_params(self, param_nr, num, unit):
        result = self.read_params(param_nr, num, unit)
        if result.isError():
            print('The unit responded with exception code {0}'.format(result.exception_code))
        else:
            for i in range(param_nr,param_nr+num):
                print('Param {0}: {1}'.format(i,result.registers[i-param_nr]))

    def readall_mon_regs(self, unit):
        address = self.cfg['units'][unit]['address']
        u_type = self.cfg['units'][unit]['unit_type']
        offset = self.cfg['unit_types'][u_type]['reg_offset']
        mon_regs  = self.cfg['unit_types'][u_type]['mon_regs']
        for reg in mon_regs:
            reading = self.client.read_holding_registers(reg+offset,1,unit=address)
            if reading.isError():
                mon_regs[reg]['value'] = 'Error code {:02d}'.format(reading.exception_code)
            else:
                mon_regs[reg]['value'] = reading.registers[0]
        return mon_regs  

    def displayall_mon_regs(self, unit):
        result = self.readall_mon_regs(unit)
        for reg in result:
            print('{0}: {1}'.format(reg,result[reg]['desc']))
            print("  register value: {0}".format(result[reg]['value']))
            if result[reg]['unit']=='binary':
                display_binary_reg(result[reg], 16)
            elif type(result[reg]['scale']) in [int, float]:
                print("  representation: {0} [{1}]".format(result[reg]['value']*result[reg]['scale'],result[reg]['unit']))

    def read_mon_group(self, group, unit, log=False): # group and unit as in the config file
        #if not group in self.cfg['units'][unit]['monitor_groups']:
        #    return None
        group = self.cfg['units'][unit]['monitor_groups'][group]['measurements']
        for meas in group:
            logging.debug('reading measurement {} from unit {}'.format(meas, unit))
            meas_type = group[meas]['type']
            src = group[meas]['src']
            if type(src) == dict:
                src_list = [src]
            elif type(src) == list:
                src_list = src
            tags = {}
            meas_name = meas
            if 'tags' in group[meas]:
                tags = group[meas]['tags']
                if 'meas_name' in group[meas]:
                    meas_name = group[meas]['meas_name']
            values = {}
            for src_reg in src_list:
                new_values = self.read_mon_measurement(src_reg, unit, meas_type)
                if type(new_values) == dict:
                    values.update(new_values)
                #else: #it must be a str and it must have been an error
                    # do sth
            if len(values.keys()) == 0:
                continue
            if log:
                #point = {'measurement': meas, 'tags': {'location': self.plant}, 'time': datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'), 'fields': values}
                #self.influx_c.write_points([point])
                tags.update({'unit':unit})
                self.telegraf_c.metric(meas_name, values, tags=tags)
        return group

    def read_mon_measurement(self, src, unit, meas_type):
        reg = src['reg']
        #if type(reg) == str:
        #    (regs,ops) = split_op_string(reg)
        reading = self.read_mon_reg(reg, unit)
        if type(reading) == str:
            return reading
        if meas_type == 'binary':
            bits = src['bits']
            for bit in bits:
                mask = 1<<bit
                masked_reg = reading&mask
                bits[bit]['value'] = True if masked_reg else False 
            return {bits[bit]['bit_lbl']:bits[bit]['value'] for bit in bits}
        elif meas_type == 'gauge':
            if 'fvals' in src:                # forbidden values (values known to come from errors)
                if reading in src['fvals']:
                    return None
            src['value'] = reading
            meas_offset = 0
            meas_scale = 1
            if 'field' in src:
                field_name = src['field']
            else:
                field_name = 'value'
            if 'offset' in src:
                meas_offset = src['offset']
            if 'scale' in src:
                meas_scale = src['scale']
            return {field_name: src['value']*meas_scale-meas_offset}

    def read_mon_reg(self, reg, unit):
        u_type = self.cfg['units'][unit]['unit_type']
        mon_reg_props = self.cfg['unit_types'][u_type]['mon_regs'][reg]
        range_prop = mon_reg_props['range']
        regs = self.read_unit_reg(reg, unit)
        if type(regs) == str:
            return regs
        already_scaled = False
        if len(regs)<2:
            if 'float' in range_prop[0:5]:
                reg_bytes = nbyte_padding(bin(regs[0]),2)
                reading = bitstring.BitArray(reg_bytes).float
            else:
                reading = regs[0]
        else:
            if 'float' in range_prop[0:5]:
                float_as_int = 0
                if any(x in range_prop for x in ['high word', 'low word']):
                    if 'low word' in range_prop:
                        regs = [regs[i] for i in [1,0]]
                    float_as_int = (regs[0]<<16)+regs[1]
                else:
                    logging.debug('The register is neither a low word nor a high word in a paired modbus register {} from unit {}'.format(src['reg'], unit))
                reg_bytes = nbyte_padding(bin(float_as_int),4)
                reading = bitstring.BitArray(reg_bytes).float
            else:
                regpair = mon_reg_props['reg_pair']
                mon_regpair_props = self.cfg['unit_types'][u_type]['mon_regs'][regpair]
                reading = regs[0]*mon_reg_props['scale']+regs[1]*mon_regpair_props['scale']
                already_scaled = True
        if type(mon_reg_props['scale']) != str and not already_scaled:
            reading = reading*mon_reg_props['scale']
        if 'offset' in mon_reg_props:
            reading = reading-mon_reg_props['offset']
        if 'float' in range_prop[0:5]:
            reading = float(reading)
        return reading

    def read_unit_reg(self, reg, unit): # takes into account whether a reg has low and high words (is paired and you cannot avoid reading its pair)
        u_type = self.cfg['units'][unit]['unit_type']
        mon_reg_props  = self.cfg['unit_types'][u_type]['mon_regs'][reg]
        if 'reg_pair' not in mon_reg_props:
            reading = self.read_modbus_regs(reg, unit, 1)
            return reading
        else:
            regpair = mon_reg_props['reg_pair']
            reg_d = regpair-reg
            if reg_d in [-1, 1]:
                r_reg = reg+min(reg_d,0)
                readings = self.read_modbus_regs(r_reg, unit, 2)
                if type(readings) == str:
                    return readings
                if reg_d == -1:
                    readings = [readings[i] for i in [1,0]]
            else:
                readings = [self.read_modbus_regs(i, unit, 1) for i in [reg, regpair]]
                for el in readings:
                    if type(el) == str:
                        return el
            return readings

    def read_modbus_regs(self, reg, unit, num_reg):
        unit_addr = self.cfg['units'][unit]['address']
        u_type = self.cfg['units'][unit]['unit_type']
        offset = self.cfg['unit_types'][u_type]['reg_offset']
        monreg_type = self.cfg['unit_types'][u_type]['mon_reg_type'] # either holding or input
        reg_addr = reg+offset
        if monreg_type == 'holding':
            reading = self.client.read_holding_registers(reg_addr,num_reg,unit=unit_addr)
        elif monreg_type == 'input':
            reading = self.client.read_input_registers(reg_addr,num_reg,unit=unit_addr)
        if not reading.isError():
            return reading.registers
        else:
            if hasattr(reading, 'exception_code'):
                error = 'Modbus Exception code {:02d}'.format(reading.exception_code)
            else:
                error = 'Modbus Exception message: {}'.format(reading.string)
            logging.error('An exception occurred when reading register {} from unit on unit_addr {}: {}'.format(reg, unit_addr, error))
            #raise Exception(error)
            return error

    def display_mon_group(self, group, unit):
        result = self.read_mon_group(group, unit)
        for meas in result:
            msrmnt = result[meas]
            if msrmnt['type'] == 'gauge':
                print('{0}: {1}'.format(meas,msrmnt['src']['value']))
            elif msrmnt['type'] == 'binary':
                print(printable_bits(msrmnt['src']))

    def log_mon_group(self, group, unit):
        result = self.read_mon_group(group, unit, log=True)

    def log_mon_groups(self, unit):
        groups = self.cfg['units'][unit]['monitor_groups']
        for group in groups:
            if groups[group]['active']:
                result = self.read_mon_group(group, unit, log=True)

    def findall_regs_to_read(self, unit):
        srcs=[]
        unit_conf=self.cfg['units'][unit]
        for mongrp in unit_conf['monitor_groups']:
            msrmnts=unit_conf['monitor_groups'][mongrp]['measurements']
            srcs.extend([msrmnts[m]['src'] for m in msrmnts])
        # collect all regs
        regs=[]
        for s in srcs:
            if type(s)!=list:
                s=[s]
            for ss in s:
                if type(ss['reg'])==int:
                    regs.append(ss['reg'])
                elif type(ss['reg'])==str:
                    r = re.split('[^0-9]',ss['reg'])
                    regs.extend([int(rr) for rr in r])

        return sorted(regs)

def cons_ranges(data):
    ranges=[]
    for k,g in groupby(enumerate(data), lambda x:x[0]-x[1]):
        group = (map(itemgetter(1),g))
        group = list(map(int,group))
        ranges.append((group[0],group[-1]))
    return ranges

def nbyte_padding(s,n):
    l=len(s)
    if(l)>8*n+1:
        return '0b'+s[-8*n:]
    else:
        return '0b'+'0'*(8*n+2-l)+s[2-l:]

def read_bits_to_disp(bits_desc, bits):
    for bit in bits_desc:
        value = '1' if bits_desc[bit]['value'] else '0'
        printable = bits_desc[bit]['bit_lbl']+':\t'+value
        bits.append(printable)

def printable_bits(src):
    bits = []
    if type(src) == dict:
        bits_desc = src['bits']
        read_bits_to_disp(bits_desc, bits)
    if type(src) == list:
        for src_reg in src:
            read_bits_to_disp(src_reg['bits'], bits)
    return '{0}:\n    {1}'.format(meas,'\n    '.join(bits))

def display_binary_reg(reg_dict, reg_len):
    names = reg_dict['scale'].split(',')
    binario = ('{:0>'+str(reg_len)+'}').format(bin(reg_dict['value'])[2:])
    values = [binario[i] for i in range(len(binario))]
    print('  |'+'|'.join([' {:>4} '.format(names[i]) for i in range(len(names))])+'|')
    print('  |'+'|'.join([' {:>4} '.format(values[i]) for i in range(len(values))])+'|')

def split_op_string(a,chars='+-'):
    sep=chars[0]
    b=a.split(sep)
    n_sep=chars[1:len(chars)]
    if len(n_sep)==0:
        res_1=b
        res_2=sep*(len(b)-1)
    else:
        res_1=[]
        res_2=''
        for chunk in b:
            (res_11,res_22)=split_string(chunk,n_sep)
            res_1.extend(res_11)
            res_2=res_2+res_22+sep
        res_2=res_2[0:len(res_2)-1]
    return (res_1,res_2)

def main(args):

    logging.basicConfig(filename=args['logfile'], format='%(asctime)s %(levelname)s %(message)s', level=eval('logging.'+args['loglevel']))
    
    logging.info('Starting monitoring process...')
    master = drstk_ModbusClient(device=args['device'], config_file=args['config'])
    if not master.connection:
        logging.error('Could not establish connection :(')
        print('Could not establish connection... exiting :(')
        sys.exit(1)
    else:
        logging.info('Successfully connected to the bus! :)')
    while True:
        time.sleep(master.period)
        for unit in master.cfg['units']:
            master.log_mon_groups(unit)

if __name__ == '__main__': 
    parser = argparse.ArgumentParser(description='Monitorea mediante puerto serial y protocolo Modbus RTU las variables de estado del proceso de la planta en cuestión que son controladas por los equipos configurados en el archivo config.yaml. Reporta las métricas a un servicio telegraf o influxDB de acuerdo a la configuración.')
    # intended options to consider
    parser.add_argument('-c', '--config', help='El archivo de configuración YAML con los dispositivos a monitorear, sus direcciones, variables a monitorear, nombres de métricas, etc.', default='config.yaml', required=False)
    parser.add_argument('-d', '--device', help='Dispositivo serial a usar.', default='/dev/ttyUSB0', required=False)
    parser.add_argument('-e', '--equipment', help='El archivo de configuración YAML con los modelos de equipos conocidos y sus detalles internos', required=False)
    parser.add_argument('-l', '--logfile', default='serious_serial_monitor.log', help='The file to log the activity of the script', required=False)
    parser.add_argument('-L', '--loglevel', default='INFO', help='The logging level of the script.', required=False)
    parser.add_argument('-v', '--verbose', default=False, help='Print detailed information about what the script is doing.', required=False)
    args = vars(parser.parse_args())
    main(args)

#def human_read_f700_params(param_nr, num, unit):
#  return read_f700_params(param_nr, num, unit) # remember to do some magic with the knowledge we have about f700 parameters
