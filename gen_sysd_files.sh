while getopts c: option
do
    case "${option}"
        in
        c) CONFFILE=${OPTARG};;
    esac
done

echo "[Unit]
Description=Serious serial monitor of industrial equipment
After=syslog.target

[Service]
Type=simple
User=$USER
Group=$USER
WorkingDirectory=$(pwd)
ExecStart=$(pyenv which python) $(readlink -f SeriousSerialMonitor.py) -c $CONFFILE
StandardOutput=syslog
StandardError=syslog

[Install]
WantedBy=multi-user.target" > serious-serial-monitor.service
# WantedBy=multi-user.target" > /usr/lib/systemd/system/serious-serial-monitor.service

echo "enable serious-serial-monitor" > 20-serious-serial-monitor.preset
# echo "enable serious-serial-monitor" > /usr/lib/systemd/system-preset/20-serious-serial-monitor.preset
